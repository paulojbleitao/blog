module MarkdownRenderer exposing (customRenderer)

import Html exposing (Html)
import Html.Attributes as Attr
import Markdown.Html
import Markdown.Parser
import Markdown.Renderer

-- i have no idea why using the defaultHtmlRenderer directly doesn't work
defaultHtmlRenderer : Markdown.Renderer.Renderer (Html msg)
defaultHtmlRenderer = Markdown.Renderer.defaultHtmlRenderer
--

customRenderer : Markdown.Renderer.Renderer (Html msg)
customRenderer =
    { defaultHtmlRenderer
        | html =
            Markdown.Html.oneOf
                [ Markdown.Html.tag "img"
                    (\src alt width children ->
                        img src alt width children)
                    |> Markdown.Html.withAttribute "src"
                    |> Markdown.Html.withAttribute "alt"
                    |> Markdown.Html.withAttribute "width",
                  Markdown.Html.tag "p"
                    (\align children ->
                        p align children)
                    |> Markdown.Html.withAttribute "align"
                ]
    }

img : String -> String -> String -> List (Html msg) -> Html msg
img src alt width _ =
    Html.img [ Attr.src src, Attr.alt alt, Attr.width (Maybe.withDefault 670 (String.toInt width)) ] []

p : String -> List (Html msg) -> Html msg
p align children =
    Html.p [ Attr.align align ] children
