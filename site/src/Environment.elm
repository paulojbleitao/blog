module Environment exposing (..)

import Url

websiteName : String
websiteName = "laranja.land"

apiUrl : String
apiUrl = "http://api.laranja.land:5000/"

baseUrl : Url.Url
baseUrl = Url.Url Url.Http "localhost" (Just 80) "/" Nothing Nothing

urlFromPath : String -> Url.Url
urlFromPath path =
    Maybe.withDefault baseUrl (Url.fromString ("http://localhost:80/" ++ path))

