module Date exposing (..)

import Time exposing (..)

brt : Zone
brt = customZone (-3 * 60) []

toPortugueseMonth : Month -> String
toPortugueseMonth month =
    case month of
        Jan -> "Janeiro"
        Feb -> "Fevereiro"
        Mar -> "Março"
        Apr -> "Abril"
        May -> "Maio"
        Jun -> "Junho"
        Jul -> "Julho"
        Aug -> "Agosto"
        Sep -> "Setembro"
        Oct -> "Outubro"
        Nov -> "Novembro"
        Dec -> "Dezembro"

secondsToPosix : Int -> Posix
secondsToPosix time = millisToPosix (time * 1000)

formatDate : Int -> String
formatDate time =
    String.fromInt (toDay brt (secondsToPosix time)) ++ " de " ++
    (toPortugueseMonth (toMonth brt (secondsToPosix time))) ++ " de " ++
    String.fromInt (toYear brt (secondsToPosix time)) ++ " às " ++
    formatNumber (toHour brt (secondsToPosix time)) ++ ":" ++
    formatNumber (toMinute brt (secondsToPosix time))

formatNumber : Int -> String
formatNumber n =
    if n <= 9 then "0" ++ String.fromInt(n)
    else String.fromInt(n)
