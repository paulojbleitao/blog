module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Url
import Date
import Http
import Result
import Environment
import Json.Decode as D
import Element exposing (Element)
import Element.Border
import Element.Font
import Element.Background
import Element.Input
import Markdown.Parser
import Markdown.Renderer
import MarkdownRenderer

main =
  Browser.application {
        init = init,
        update = update,
        view = view,
        subscriptions = subscriptions,
        onUrlChange = UrlChanged,
        onUrlRequest = LinkClicked
    }


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


-- MODEL

type alias Model = {
        posts: List Post,
        flags: Flags,
        url: Url.Url,
        key: Nav.Key
    }
type alias Flags = {
        width: Int,
        height: Int
    }
type alias Post = {
        title: String,
        description: String,
        body: String,
        url: String,
        author: String,
        date: Int
    }

init : Flags -> Url.Url -> Nav.Key -> (Model, Cmd Msg)
init flags url key = (
        Model [] flags url key,
        getPosts
    )


-- UPDATE

type Msg = GotPosts (Result Http.Error (List Post))
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        GotPosts result ->
            case result of
                Ok posts -> ({ model | posts = List.append posts model.posts }, Cmd.none)
                Err error  -> (model, Cmd.none)
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    (model, Nav.pushUrl model.key (Url.toString url))
                Browser.External href ->
                    (model, Nav.load href)
        UrlChanged url -> ({ model | url = url }, Cmd.none)

getPosts : Cmd Msg
getPosts =
    Http.get {
        url = Environment.apiUrl,
        expect = Http.expectJson GotPosts postsDecoder
    }

postsDecoder : D.Decoder (List Post)
postsDecoder =
    D.field "posts" (D.list postDecoder)

postDecoder : D.Decoder Post
postDecoder =
    D.map6 Post
        (D.field "title" D.string)
        (D.field "description" D.string)
        (D.field "body" D.string)
        (D.field "url" D.string)
        (D.field "author" D.string)
        (D.field "date" D.int)

-- VIEW

type Text =
    GeneralText
    | PostTitleText
    | PostAuthorAndDateText

fontSize : Text -> Flags -> Int
fontSize text flags =
    let
        device = Element.classifyDevice flags
    in
        case (text, device.class) of
            (GeneralText, Element.Phone) -> 40
            (GeneralText, _) -> 20
            (PostTitleText, Element.Phone) -> 56
            (PostTitleText , _) -> 36
            (PostAuthorAndDateText, Element.Phone) -> 26
            (PostAuthorAndDateText, _) -> 16

view : Model -> Browser.Document Msg
view model = {
        title = "blog",
        body =
            [ Element.layoutWith
                { options = [ Element.focusStyle (Element.FocusStyle Nothing Nothing Nothing) ] }
                [ (Element.Background.color (Element.rgb255 0 0 0)),
                  (Element.Font.color (Element.rgb255 255 255 255)) ]
                (routes model) ]
    }

routes : Model -> Element Msg
routes model =
    case model.url.path of
        "/" -> homePage model
        _ -> postPage model

homePage : Model -> Element Msg
homePage model = Element.column
    [ (Element.width Element.fill),
      (Element.height Element.fill),
      (Element.spacing 50) ]
    (List.append [ navbar model.flags ] (List.map (singlePostPreview model.flags) model.posts))

filteredPost : Model -> Post
filteredPost model = Maybe.withDefault (Post "" "" "" "" "" 0) (List.head (List.filter (\post -> "/" ++ post.url == model.url.path) model.posts))

postPage : Model -> Element Msg
postPage model = Element.column
    [ (Element.width Element.fill),
      (Element.height Element.fill),
      (Element.spacing 50) ]
    ([ navbar model.flags, fullPost (filteredPost model) model.flags ])

fullPost : Post -> Flags -> Element Msg
fullPost post flags= Element.column
    [ (Element.width (Element.px 750)),
      Element.centerX ]
    [ postTitle post flags,
      postAuthorAndDate post flags,
      postBody post flags ]

-- código da documentação: https://package.elm-lang.org/packages/dillonkearns/elm-markdown/latest/Markdown-Parser
render renderer markdown =
    markdown
        |> Markdown.Parser.parse
        |> Result.mapError deadEndsToString
        |> Result.andThen (\ast -> Markdown.Renderer.render renderer ast)

deadEndsToString deadEnds =
    deadEnds
        |> List.map Markdown.Parser.deadEndToString
        |> String.join "\n"
-- fim do código da documentação

postBody : Post -> Flags -> Element Msg
postBody post flags = Element.paragraph
    [ (Element.paddingXY 40 20),
      Element.Font.size (fontSize GeneralText flags),
      Element.Font.family
        [ (Element.Font.typeface "FK Roman Standard") ] ]
    (case (render MarkdownRenderer.customRenderer post.body) of
        Result.Ok markdownInHtml -> List.map Element.html markdownInHtml
        Result.Err list -> [Element.text list])

navbar : Flags -> Element Msg
navbar flags =
    Element.el
        [ (Element.width Element.fill),
          (Element.paddingXY 20 10),
          (Element.Border.widthEach { bottom = 2, left = 0, right = 0, top = 0}),
          (Element.Border.color (Element.rgb255 255 255 255)),
          (Element.Font.color (Element.rgb255 255 172 65)) ]
        (Element.Input.button [] {
           onPress = Just (LinkClicked (Browser.Internal Environment.baseUrl)),
           label = Element.paragraph
                [ Element.Font.size (fontSize GeneralText flags) ]
                [ Element.text Environment.websiteName ]
        })

singlePostPreview : Flags -> Post -> Element Msg
singlePostPreview flags post =
   Element.column
    [ (Element.width (Element.px 750)),
      Element.centerX,
      (Element.paddingXY 0 25),
      (Element.Border.width 2),
      (Element.Border.rounded 10) ]
        [ postTitle post flags,
          postAuthorAndDate post flags,
          postPreviewBody post flags ]

postTitle : Post -> Flags -> Element msg
postTitle post flags = Element.paragraph
    [ (Element.paddingXY 20 0),
      (Element.Font.size (fontSize PostTitleText flags)) ]
    [ Element.text post.title ]

postAuthorAndDate : Post -> Flags -> Element msg
postAuthorAndDate post flags = Element.row
    [ (Element.paddingXY 20 0),
      (Element.spacingXY 10 10),
      (Element.Font.size (fontSize PostAuthorAndDateText flags)) ]
    [Element.text ((Date.formatDate post.date) ++ " por " ++ post.author) ]

postPreviewBody : Post -> Flags -> Element Msg
postPreviewBody post flags = Element.paragraph
    [ (Element.paddingXY 40 20),
      Element.Font.size (fontSize GeneralText flags),
      Element.Font.family
        [ (Element.Font.typeface "FK Roman Standard") ] ]
    [ Element.text (post.description ++ " "),
      readMoreButton post.url ]

readMoreButton : String -> Element Msg
readMoreButton url = Element.Input.button
    [ Element.Font.color (Element.rgb255 255 172 65),
      Element.Font.underline ]
    { onPress = Just (LinkClicked (Browser.Internal (Environment.urlFromPath url))),
      label = Element.text "Ler mais" }
