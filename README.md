# blog

This is my self-hosted blog implementation. It was partly motivated by a desire to avoid big centralized solutions, partly to get back into programming and to learn Elm which I had been putting off for a while, and partly for fun. :)

It consists of a server and a website. The server is written in Python with Flask and the website is written in Elm.

## Running for development

### Front-end
With Elm installed, simply run `elm reactor` and select `Main.elm`.

### Back-end
You need poetry and mongodb installed.

To start mongodb:
```
mongod --port 27017 --dbpath ~/.local/share/mongodb/blog
```

To run the server, first `poetry install` to install the dependencies. Then, do either `poetry shell` followed by `flask run` or `poetry run flask run`.

## Running for production

You need to install `docker`, `docker-buildx` and `docker-compose`.

You also need to adjust some parameters: the database's username and password, in the `compose.yaml` file and in the `server/blog/db.py` file; also in the `compose.yaml` file, you must change the IPs from 127.0.0.1 to whatever your machine's IP is; and in the `site/src/Environment.elm` file, adjust the urls to your server's url and your website's url.

Then, simply running `docker compose up` deploys the blog.
