# Server

## Posts endpoints
<!-- template: https://gist.github.com/azagniotov/a4b16faf0febd12efbc6c3d7370383a6  -->

<details>
<summary>Post format</summary>

* Author: the author's name
* Body: the post itself
* Description: a short description that will be displayed on the home page
* Title: the post's title
* Url: the name that will be displayed at the end of the url. example: `blog.laranja.land/post`
* Refs (optional): this is unused for now, but was meant to be the reference list which would appear at the end of the post
</details>

<details>
<summary><code>GET</code> <code><b>/</b></code> <code>(gets every post from the database)</code></summary>

* Parameters: none
* Responses: 200 - returns every post in a list
</details>


<details>
<summary><code>GET</code> <code><b>/post-url</b></code> <code>(gets a single post from the database)</code></summary>

* Parameters: none
* Responses: 200 - returns a single post as a json
</details>


<details>
<summary><code>POST</code> <code><b>/</b></code> <code>(adds a post to the database)</code></summary>

* Parameters: a post in a json format as specified before
* Headers: "Authorization: Bearer JWT"
* Responses:
* * 201 - "ok"
* * 409 - "post already exists"
</details>

<details>
<summary><code>PUT</code> <code><b>/post-url/edit</b></code> <code>(edits a post)</code></summary>

* Parameters: a json with the fields you want to edit
* Headers: "Authorization: Bearer JWT"
* Responses:
* * 204 - "ok"
* * 404 - "not found"
</details>

<details>
<summary><code>DELETE</code> <code><b>/post-url/delete</b></code> <code>(deletes a post)</code></summary>

* Parameters: none
* Headers: "Authorization: Bearer JWT"
* Responses: 204 - "ok"
</details>

## Authorization endpoints

<details>
<summary><code>POST</code> <code><b>/login</b></code> <code>(logs in)</code></summary>

* Parameters: a json containing the fields "username" and "password"
* Responses:
* * 200 - a json containing the JWT
* * 404 - "not found"
</details>
