from flask import Response, Blueprint, request, json
from . import db
from . import jwt
import functools

bp = Blueprint('auth', __name__)
users = db.db.users

def login_required(f):
    @functools.wraps(f)
    def secure_function(*args, **kwargs):
        token = request.headers.get('Authorization')
        if token and jwt.validate_jwt(token.split()[1]):
            return f(*args, **kwargs)
        return 'unauthorized', 401

    return secure_function

@bp.route('/login', methods=['POST'])
def login():
    user = request.get_json()
    # TODO: não parece nada seguro
    if list(users.find({ 'username': user['username'], 'password': user['password'] })):
        response = { 'jwt': jwt.generate_jwt(user['username']) }
        response = Response(json.dumps(response))
        response.content_type = 'application/json'
        return response, 200
    return 'not found', 404
