import hmac
import json
import time
import base64
import hashlib

# should probably store this somewhere else
SECRET = 'fpdF8GiWSf341Rh7OzfZgzghdlmPweES'

def generate_jwt(user: str):
    header = { 'alg': 'HS256', 'typ': 'JWT' }
    payload = {
        'user': user,
        'iat': time.time(),
        'exp': time.time() + 3600 # expires in an hour
    }
    header_bytes = json.dumps(header).encode('utf-8')
    header_encoded = base64.urlsafe_b64encode(header_bytes)
    payload_bytes = json.dumps(payload).encode('utf-8')
    payload_encoded = base64.urlsafe_b64encode(payload_bytes)
    signature = hmac.new(SECRET.encode('utf-8'),
                header_encoded + '.'.encode('utf-8') + payload_encoded,
                hashlib.sha256).hexdigest()
    return header_encoded.decode() + '.' + payload_encoded.decode() + '.' + signature

def validate_jwt(jwt: str):
    header, payload, signature = jwt.split('.')
    new_signature = hmac.new(SECRET.encode('utf-8'),
                    header.encode('utf-8') + '.'.encode('utf-8') + payload.encode('utf-8'),
                    hashlib.sha256).hexdigest()
    payload_decoded = json.loads(base64.urlsafe_b64decode(payload))
    if new_signature == signature and time.time() < payload_decoded['exp']:
        return True
    return False
