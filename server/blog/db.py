from pymongo import MongoClient

# docker compose sets up a network for the containers, "mongodb" is what the mongodb container is named
client = MongoClient('mongodb', 27017,
                     username='user',
                     password='password',
                     authSource='admin',
                     authMechanism='SCRAM-SHA-256')
db = client.blog
