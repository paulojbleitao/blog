from flask import Flask
from . import posts
from . import auth

def create_app():
    app = Flask(__name__)
    app.register_blueprint(posts.bp)
    app.register_blueprint(auth.bp)
    return app

app = create_app()
