from flask import Response, Blueprint, request, json
from . import db
from . import auth
import time

bp = Blueprint('posts', __name__)
posts = db.db.posts

@bp.route('/<post_url>', methods=['GET'])
def get_post(post_url):
    post = posts.find({ 'url': post_url })[0]
    del post['_id']
    # TODO: apenas para desenvolvimento, consertar o CORS para produção
    post['date'] = int(post['date'])
    response = Response(json.dumps(post))
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.content_type = 'application/json'
    return response, 200

@bp.route('/', methods=['GET'])
def get_several_posts():
    response = { 'posts': [] }
    for p in posts.find():
        del p['_id']
        p['date'] = int(p['date'])
        response['posts'].append(p)

    response = Response(json.dumps(response))
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.content_type = 'application/json'
    return response, 200

@bp.route('/', methods=['POST'])
@auth.login_required
def add_post():
    post = request.get_json()
    similar_posts = posts.find({ '$or': [{ 'url': post['url'] },
                               { 'title': post['title'] },
                               { 'body': post['body'] }]})
    try:
        if similar_posts.next():
            return 'post already exists', 409
    except:
        post['date'] = int(time.time())
        posts.insert_one(post)
        return 'ok', 201


@bp.route('/<post_url>/edit', methods=['PUT'])
@auth.login_required
def edit_post(post_url):
    edited_post = request.get_json()
    edited_post['lastUpdated'] = int(time.time())
    if posts.find({ 'url': post_url }).next():
        posts.update_one({ 'url': post_url },
                        { '$set': edited_post })
        return 'ok', 204
    return 'not found', 404


@bp.route('/<post_url>/delete', methods=['DELETE'])
@auth.login_required
def delete_post(post_url):
    posts.delete_one({ 'url': post_url })
    return 'ok', 204
